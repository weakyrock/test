$(document).ready(function() {
    // [START] sticky navigation
    if ($(window).width() > 1199) {
        var navbar_height = $('.navbar').outerHeight();

        $(window).scroll(function() {
            if ($(this).scrollTop() > 500) {
                $('.navbar-wrap').css('height', navbar_height + 'px');
                $('#navbar_top').addClass("fixed-top");

            } else {
                $('#navbar_top').removeClass("fixed-top");
                $('.navbar-wrap').css('height', 'auto');
            }
        });
    }
    // [END] sticky navigation

    // [START] FAQs tab
    $('.question-list a').click(function() {
        $('.question-list a').removeClass('active');
        $(this).addClass('active');
        $('.anwContent').removeClass('show');
        var targetTab = $(this).attr('tab');
        $('#' + targetTab).addClass('show');
    });
    // [END] FAQs tab
    $('#main_nav .nav-link').click(function() {
        $('#main_nav').toggleClass('show');
    });

    // [START] show datepicker if input has class datepicker
    if ($("input").hasClass("datepicker")) {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true,
            changeYear: true,
            yearRange: "-20:+0"
        });
    }
    // [END] show datepicker if input has class datepicker

});

// [START] Simple validator for newsletter
$(".ipText").on("input", function() {
    console.log($('#address').val().length);
    $(".error").hide();
    if ($('#email').val().length > 0 && $('#address').val().length == 0) {
        $('#btn-submit').addClass('active');
    } else {
        $('#btn-submit').removeClass('active');
    }
});
// [END] Simple validator for newsletter

// [START] Submit action for newsletter form
$('#btn-submit').click(function() {
    $(".error").hide();
    var hasError = false;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    var emailaddressVal = $("#email").val();
    if (emailaddressVal == '') {
        $("#email").after('<span class="error">Vui lòng nhập Email của bạn</span>');
        hasError = true;
    } else if (!emailReg.test(emailaddressVal)) {
        $("#email").after('<span class="error">Vui lòng nhập địa chỉ Email hợp lệ</span>');
        hasError = true;
    }
    if ($('#fullName').val().length == 0) {
        $("#fullName").after('<span class="error">Vui lòng nhập Họ và Tên của bạn</span>');
        hasError = true;
    }
    if ($("#companyName").length && $('#companyName').val().length == 0) {
        $("#companyName").after('<span class="error">Vui lòng nhập Tên công ty</span>');
        hasError = true;
    }
    if ($("#phoneNumber").length && $('#phoneNumber').val().length == 0) {
        $("#phoneNumber").after('<span class="error">Vui lòng nhập Số điện thoại của bạn</span>');
        hasError = true;
    }

    // ADD SCRIPT FOR RUN POST
    if (hasError == true) {
        return false;
    } else if (hasError == false) {
        $("#btn-submit").before('<div class="success">Foxpay đã ghi nhận thông tin của bạn. Xin cảm ơn.</div>');
        $("#btn-submit").slideUp();
        //*****PHP SUBMIT CODE HERE//*****
    }

});
// [END] Submit action for newsletter form

// [START] Tab function for service page
function tab_focus(n) {
    $('.service-category a').removeClass('active');
    $('a#' + n + '-tab').addClass('active');
    $('.tab-pane').removeClass('active show');
    $('#' + n).addClass('active show');
}
// [END] Tab function for service page

// [START] User select filter service
function select_filter(n) {
    //Show title
    $('#tabsPartner .title-section.sub').css('display', 'block');
    // [START]Refresh searching
    $("#hoadon .filterSection input").val("");
    $(".partner-search").removeAttr("style");
    $(".partner-item").removeAttr("style");
    $(".partner-item img").removeAttr("style");
    $(".partner-item span").removeAttr("style");
    // [END]Refresh searching

    $('.item-select a').removeClass('selected');
    $('#' + n).addClass('selected');
    $('.infoTab').hide();
    $('#' + n + 'Information').slideDown();

}
// [END] User select filter service

// [START] User select partner form
function select_partner(n) {
    $('.item-partner a').removeClass('selected');
    $('#' + n).addClass('selected');
    $('#tabsPartnerForm > div').slideUp();
    $('#' + n + 'Tab').slideDown();

}
// [END] User select partner form

// [START] Service Partners searching by Trung
$("#hoadon .filterSection input").on("keyup", function() {
    var targetID = $(this).attr('id');
    var attSearch = $("#" + targetID).attr('for');
    var value = $("#" + targetID).val().toLowerCase();
    $("#hoadon #" + attSearch + "Information " + ".partner-list *").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});
// [END] Service Partners searching by Trung

// [START] validate just number in phoneNumber
function validate_numberic(evt) {
    var theEvent = evt || window.event;
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    //just numberic can input in this.
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}
// [END] validate just number in phoneNumber

// [START] Function define webview for display aboutus page in App. 
window.onload = function() {
    let paramView = new URLSearchParams(window.location.search).get('v');
    if (paramView == "webview") {
        $('body').addClass('webview');
    } else {
        $('body').addClass('normal');
    }
}
// [END] Function define webview for display aboutus page in App.

// [START] read more script
$(".readmore").on("click", function() {
    var txt = $(".full-desc").is(':visible') ? 'Xem thêm' : 'Rút gọn';
    $(this).text(txt);
    $(this).parent().find('.full-desc').slideToggle();
});
// [END] read more script

// [START] function callErorr/clearError for show/hide error of input field
// id = id name of field || error = string content of this error
function callError(id, error) {
    $('#' + id).parent().addClass('errorWrap');
    $('#' + id).after('<span class="error' + ' ' + id + '">' + error + '</span>');
    $('.mainCTA').addClass('disabled');
}

function clearError(id) {
    $('.' + id).slideUp();
    $('.mainCTA').removeClass('disabled');
}
// [END] function callErorr/clearError for show/hide error of input field

// [START] Function for load image then show preview in form
function showPreview(input, f) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#' + f).attr('src', e.target.result);
            $('#' + f).parent().find('.desc').hide();
            $('#' + f).parent().parent().addClass('selected');

        };
        reader.readAsDataURL(input.files[0]);
    }
}
// function for remove preview image
$('.removeImg').click(function() {
    $(this).parent().removeClass('selected');
    $(this).parent().find('.desc').show();
    $(this).parent().find('.holder').attr('src', 'assets/img/img_holder.svg');
});
// [END] Function for load image then show preview in form

/* Vietnamese localization for the jQuery UI date picker plugin. */
jQuery(function($) {
    $.datepicker.regional["vi-VN"] = {
        closeText: "Đóng",
        prevText: "Trước",
        nextText: "Sau",
        currentText: "Hôm nay",
        monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
        monthNamesShort: ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],
        dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
        dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
        dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        weekHeader: "Tuần",
        dateFormat: "dd/mm/yy",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ""
    };

    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
});