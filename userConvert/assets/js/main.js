// script filter just number in inputs
function validate_numberic(evt) {
    var theEvent = evt || window.event;
    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
        // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    //just numberic can input in this.
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

//[START] Input First Password
$('#inputPassword').on("input", function() {
    fillPassword();
    var lInput = $(this).val().replace(/\s+/g, '').length;
    var lInput2 = $('#inputPasswordSe').val().replace(/\s+/g, '').length;
    var passInputFirst = this.value;
    var passInputSecond = $('#inputPasswordSe').val();
    $('.passwordView i').removeClass('active');
    closeNoti();
    switch (lInput) {
        case 1:
            $('.1').addClass('active');
            break;
        case 2:
            $('.1,.2').addClass('active');
            break;
        case 3:
            $('.1,.2,.3').addClass('active');
            break;
        case 4:
            $('.1,.2,.3,.4').addClass('active');
            break;
        case 5:
            $('.1,.2,.3,.4,.5').addClass('active');
            break;
        case 6:
            $('.1,.2,.3,.4,.5,.6').addClass('active');
            $('#inputPassword').blur();
            if (lInput == lInput2 && passInputSecond != passInputFirst) {
                callNoti("Mật khẩu không khớp");
            }
            break;
        default:
            $('.passwordView i').removeClass('active');
    }
});
//[END] Input First Password
//[START] Input Second Password
$('#inputPasswordSe').on("input", function() {
    fillPassword();
    var lInput = $(this).val().replace(/\s+/g, '').length;
    var passInputSecond = this.value;
    var passInputFirst = $('#inputPassword').val();
    closeNoti();

    $('.passwordViewSe i').removeClass('active');
    switch (lInput) {
        case 1:
            $('.11').addClass('active');
            break;
        case 2:
            $('.11,.22').addClass('active');
            break;
        case 3:
            $('.11,.22,.33').addClass('active');
            break;
        case 4:
            $('.11,.22,.33,.44').addClass('active');
            break;
        case 5:
            $('.11,.22,.33,.44,.55').addClass('active');
            break;
        case 6:
            $('.11,.22,.33,.44,.55,.66').addClass('active');
            $('#inputPasswordSe').blur();
            // Check password
            if (passInputSecond != passInputFirst) {
                callNoti("Mật khẩu không khớp");
            }
            break;
        default:
            $('.passwordViewSe i').removeClass('active');

    }
});
//[END] Input Second Password
// [START] Script for call notification example: callNoti("Mật khẩu không khớp");
function callNoti(e) {
    $('.txtNoti').text(e);
    $('.notification').slideDown();
    $('.mainCTAW').addClass('disable');
}

function closeNoti() {
    $('.notification').slideUp();
    activeCTA();
}
// [END] Script for call notification example: callNoti("Mật khẩu không khớp");

// [START] function callError example: callError("Link xác thực mở Ví Foxpay đã hết hiệu lực. Quý khách vui lòng liên hệ với NV thu cước.")
function callError(e) {
    $('.txtNoti').text(e);
    $('.notification').slideDown();
    $('.mainCTAW').addClass('disable');
    $(".password").prop('disabled', true);
}
// [END] function callError

// [START] TOGGLE Main CTA button when user don't agree with Foxpay term
// Tạm thời xóa nút checkbox theo design mới
// $('#checkTerm').change(function() {
//     if (this.checked) {
//         $('.mainCTAW').removeClass('disable');
//     } else {
//         $('.mainCTAW').addClass('disable');
//     }
// });
// [END] TOGGLE Main CTA button when user don't agree with Foxpay term

// [START] Function check password to change CTA status
function activeCTA() {
    var passInputFirst = $('#inputPassword').val();
    var passInputSecond = $('#inputPasswordSe').val();
    if (passInputFirst == passInputSecond) {
        $('.mainCTAW').removeClass('disable');
    } else {
        $('.mainCTAW').addClass('disable');
    }
}
// [END] Function check password to change CTA status

// [START] Footer CTA UX (Position of Main CTA button when focus password field)
$('.password').on('focus blur', toggleFocus);

function toggleFocus(e) {
    if (e.type == 'focus') {
        $(".footC").addClass("relative");
    } else {
        $(".footC").removeClass("relative");
    }
}
// [END] Footer CTA UX (Position of Main CTA button when focus password field)

// [START]Create Modal "Điều khoản và điều kiện"
function modalCreate(mName, mButton) {
    var modal = document.getElementById(mName);
    var btn = document.getElementById(mButton);
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function() {
        modal.style.display = "block";
    }
    // span.onclick = function() {
    //     modal.style.display = "none";
    // }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}
modalCreate("modalTerm", "term");
$('.modal').click(function() {
    $(this).hide();
});
// [END]Create Modal "Điều khoản và điều kiện"

// [START] Toggle hide/show password
$('.btnToggle').click(function() {
    $('.btnToggle').toggleClass("active");
    $('.dot').toggleClass("show");
    fillPassword();
});

function fillPassword() {
    var passInputFirst = $('#inputPassword').val();
    var passInputSecond = $('#inputPasswordSe').val();
    $('.1').text(passInputFirst.slice(0, 1));
    $('.2').text(passInputFirst.slice(1, 2));
    $('.3').text(passInputFirst.slice(2, 3));
    $('.4').text(passInputFirst.slice(3, 4));
    $('.5').text(passInputFirst.slice(4, 5));
    $('.6').text(passInputFirst.slice(5, 6));

    $('.11').text(passInputSecond.slice(0, 1));
    $('.22').text(passInputSecond.slice(1, 2));
    $('.33').text(passInputSecond.slice(2, 3));
    $('.44').text(passInputSecond.slice(3, 4));
    $('.55').text(passInputSecond.slice(4, 5));
    $('.66').text(passInputSecond.slice(5, 6));
}
// [END] Toggle hide/show password

// [05-12] Update script for close banner button
$('#closeBanner').click(function(){
    $('.banner').slideUp();
});